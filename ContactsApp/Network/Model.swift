import Foundation
import UIKit

class Model {
    
    static let shared = Model()
    
    private init() {}
    
    func getProfiles(completion: @escaping ((Result<[Person], RequestError>) -> ())) {
        guard let urlComponents = URLComponents(string: "https://randomuser.me/api/?results=100") else {
            completion(.failure(.invalidUrl))
            return
        }
        var request = URLRequest(url: urlComponents.url!)
        request.httpMethod = "GET"
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            if error != nil {
                completion(.failure(.unableToComplete))
                return
            }
            guard let response = response as? HTTPURLResponse, response.statusCode == 200 else {
                completion(.failure(.invalidResponse))
                return
            }
            guard let data = data else {
                completion(.failure(.invalidData))
                return
            }
            do {
                let persons = try JSONDecoder().decode(Person.self, from: data)
                var array = [Person]()
                array.append(persons)
                completion(.success(array))
            } catch {
                completion(.failure(.invalidData))
            }
        }
        task.resume()
    }
}
