import Foundation

enum RequestError: String, Error {
    case invalidUrl = "Invalid url request. Please try again."
    case invalidResponse = "Invalid response from the server. Please try again."
    case invalidData = "The data received from the server was invalid. Please try again."
    case unableToComplete = "Unable to comlete your request. Please try again."
}
