import Foundation
import UIKit
import Network

enum ConnectionType {
    case wifi
    case cellular
    case unknown
}

class NetworkManager {
    
    static let shared = NetworkManager()
    
    let queue = DispatchQueue.global(qos: .background)
    let monitor: NWPathMonitor
    var isConnected = false
    var connectionType: ConnectionType?
    
    private init() {
        self.monitor = NWPathMonitor()
    }
    
    func startMonitoring() {
        monitor.start(queue: queue)
        monitor.pathUpdateHandler = { [weak self] path in
            DispatchQueue.main.async {
                self?.isConnected = path.status != .unsatisfied
                self?.getConnectionType(path)
            }
        }
    }
    
    func stopMonitoring() {
        monitor.cancel()
    }
    
    func getConnectionType(_ path: NWPath) {
        if path.usesInterfaceType(.wifi) {
            connectionType = .wifi
        } else if path.usesInterfaceType(.cellular) {
            connectionType = .cellular
        } else {
            connectionType = .unknown
        }
    }
}

extension UIViewController {
    func checkInternetConnection(isConnected: Bool) {
        if isConnected == false {
            let alert = UIAlertController(title: "No Internet Connection", message: "Please check your network settings and try again.", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(okAction)
            self.present(alert, animated: true, completion: nil)
        }
    }
}

