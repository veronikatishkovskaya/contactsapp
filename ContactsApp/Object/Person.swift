import Foundation
import UIKit

struct Person: Codable {
    var results: [PersonInfo]
}

struct PersonInfo: Codable {
    var name: PersonName?
    var location: PersonLocation?
    var email: String?
    var phone: String?
    var cell: String?
    var picture: PersonImage?
}

struct PersonName: Codable {
    var first: String?
    var last: String?
}

struct PersonLocation: Codable {
    var street: LocationStreet?
    var city: String?
    var state: String?
    var country: String?
}

struct LocationStreet: Codable {
    var number: Int?
    var name: String?
}

struct PersonImage: Codable {
    var large: String?
    var medium: String?
}


