import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var tableViewScreenButton: UIButton!
    @IBOutlet weak var collectionViewScreenButton: UIButton!
    
    var network = NetworkManager.shared
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableViewScreenButton.cornerRadius(radius: 15)
        collectionViewScreenButton.cornerRadius(radius: 15)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        network.startMonitoring()
    }
    
    @IBAction func goToTableViewController(_ sender: UIButton) {
        guard let storyboard = storyboard?.instantiateViewController(withIdentifier: "TableViewController") as? TableViewController else { return }
        navigationController?.pushViewController(storyboard, animated: true)
    }
    
    @IBAction func goToCollectionViewController(_ sender: UIButton) {
        guard let storyboard = storyboard?.instantiateViewController(withIdentifier: "CollectionViewController") as? CollectionViewController else { return }
        navigationController?.pushViewController(storyboard, animated: true)
    }
}
