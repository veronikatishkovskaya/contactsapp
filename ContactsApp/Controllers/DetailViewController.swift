import Foundation
import UIKit

class DetailViewController: UIViewController {
    @IBOutlet weak var personImage: UIImageView!
    @IBOutlet weak var personName: UILabel!
    @IBOutlet weak var locationCountry: UILabel!
    @IBOutlet weak var locationCity: UILabel!
    @IBOutlet weak var locationStreet: UILabel!
    @IBOutlet weak var personEmailButton: UIButton!
    @IBOutlet weak var firstPhoneNumberButton: UIButton!
    @IBOutlet weak var secondPhoneNumberButton: UIButton!
    @IBOutlet weak var backButton: UIButton!
    
    var detailInfo = [PersonInfo]()
    var imageCache = ImageCache()
    var isConnected = NetworkManager.shared.isConnected
    
    override func viewDidLoad() {
        super.viewDidLoad()
        checkInternetConnection(isConnected: isConnected)
        backButton.cornerRadius(radius: 15)
        getMoreDetails()
    }
    
    @IBAction func goBackToTableView(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    func getMoreDetails() {
        if isConnected {
            for info in detailInfo {
                guard let adress = info.location else { return }
                guard let country = adress.country else { return }
                guard let state = adress.state else { return }
                guard let city = adress.city else { return }
                guard let street = adress.street else { return }
                locationCountry.text = country + ", " + state
                locationCity.text = city
                locationStreet.text = "\(street.name ?? "")" + ", " + "\(street.number ?? 0)"
                
                guard let name = info.name else { return }
                let fullName = "\(name.first ?? "")" + " " + "\(name.last ?? "")"
                personName.text = fullName
            }
            transformImage()
            setButtons()
        }
    }
    
    func transformImage() {
        for image in detailInfo {
            guard let image = image.picture else {return}
            guard let largeImage = image.large else {return}
            if let imageUrl = URL(string: largeImage) {
                imageCache.downloadImage(url: imageUrl) { image in
                    DispatchQueue.main.async {
                        self.personImage.image = image
                        self.personImage.cornerRadius(radius: Int(self.personImage.frame.width) / 2)
                    }
                }
            }
        }
    }
    
    func setButtons() {
        for info in detailInfo {
            guard let email = info.email else {return}
            personEmailButton.setTitle("Send email to address \(email)", for: .normal)
            personEmailButton.cornerRadius(radius: 15)
            firstPhoneNumberButton.setTitle(info.phone, for: .normal)
            firstPhoneNumberButton.cornerRadius(radius: 15)
            secondPhoneNumberButton.setTitle(info.cell, for: .normal)
            secondPhoneNumberButton.cornerRadius(radius: 15)
        }
    }
    
}
extension UIView {
    func cornerRadius(radius: Int) {
        self.layer.cornerRadius = CGFloat(radius)
        self.layer.borderWidth = 1
        self.layer.borderColor = CGColor(red: 0, green: 0, blue: 0, alpha: 1)
    }
}
