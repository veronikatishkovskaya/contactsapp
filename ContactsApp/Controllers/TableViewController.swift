import UIKit
import Foundation

class TableViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var homeButton: UIButton!
    
    var profilesArray = [PersonInfo]()
    var isConnected = NetworkManager.shared.isConnected
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getData()
        homeButton.cornerRadius(radius: 15)
    }
    
    @IBAction func homeButtonPressed(_ sender: UIButton) {
        navigationController?.popToRootViewController(animated: true)
    }
    
    func getData() {
        checkInternetConnection(isConnected: isConnected)
        if isConnected {
            Model.shared.getProfiles { [weak self] result in
                DispatchQueue.main.async {
                    switch result {
                    case .success(let persons):
                        self?.updateUI(with: persons)
                    case .failure(let error):
                        self?.showAlert(with: error.rawValue)
                    }
                }
            }
        } else {
            self.finishLoading()
        }
    }
    
    func updateUI(with data: [Person]) {
        self.startLoading()
        for object in data {
            let person = object.results
            profilesArray.append(contentsOf: person)
        }
        self.tableView.reloadData()
        self.finishLoading()
    }
    
    func startLoading() {
        DispatchQueue.main.async {
            self.activityIndicator.startAnimating()
            self.view.addSubview(self.activityIndicator)
        }
    }
    
    func finishLoading() {
        DispatchQueue.main.async {
            self.activityIndicator.stopAnimating()
            self.activityIndicator.hidesWhenStopped = true
        }
    }
    
    func showAlert(with message: String) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(okAction)
            self.present(alert, animated: true, completion: nil)
        }
    }
    
}
extension TableViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return profilesArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "PersonTableViewCell", for: indexPath) as? PersonTableViewCell else {
            return UITableViewCell()
        }
        cell.configure(result: profilesArray[indexPath.row])
        cell.backgroundColor = .clear
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let detailScreen = storyboard?.instantiateViewController(withIdentifier: "DetailViewController") as? DetailViewController else {return}
        detailScreen.detailInfo.append(profilesArray[indexPath.row])
        navigationController?.pushViewController(detailScreen, animated: true)
    }
}
