import UIKit
import Foundation

class CollectionViewController: UIViewController {
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var homeButton: UIButton!
    
    var personsArray = [PersonInfo]()
    var isConnected = NetworkManager.shared.isConnected
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getModel()
        homeButton.cornerRadius(radius: 15)
    }
    
    @IBAction func homeButtonPressed(_ sender: UIButton) {
        navigationController?.popToRootViewController(animated: true)
    }
    
    func getModel() {
        checkInternetConnection(isConnected: isConnected)
        if isConnected {
            Model.shared.getProfiles { [weak self] result in
                DispatchQueue.main.async {
                    switch result {
                    case .success(let persons):
                        self?.updateUI(with: persons)
                    case .failure(let error):
                        self?.showAlert(with: error.rawValue)
                    }
                }
            }
        } else {
            self.finishLoading()
        }
    }
    
    func updateUI(with data: [Person]) {
        self.startLoading()
        for object in data {
            let person = object.results
            personsArray.append(contentsOf: person)
        }
        self.collectionView.reloadData()
        self.finishLoading()
    }
    
    func startLoading() {
        DispatchQueue.main.async {
            self.activityIndicator.startAnimating()
            self.view.addSubview(self.activityIndicator)
        }
    }
    
    func finishLoading() {
        DispatchQueue.main.async {
            self.activityIndicator.stopAnimating()
            self.activityIndicator.hidesWhenStopped = true
        }
    }
    
    func showAlert(with message: String) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(okAction)
            self.present(alert, animated: true, completion: nil)
        }
    }
   
    
}
extension CollectionViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return personsArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PersonCollectionViewCell", for: indexPath) as? PersonCollectionViewCell else {
            return UICollectionViewCell()
        }
        cell.configure(object: personsArray[indexPath.item])
        cell.backgroundColor = .clear
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let detailScreen = storyboard?.instantiateViewController(withIdentifier: "DetailViewController") as? DetailViewController else {return}
        detailScreen.detailInfo.append(personsArray[indexPath.item])
        navigationController?.pushViewController(detailScreen, animated: true)
    }
}
