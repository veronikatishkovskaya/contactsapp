import UIKit

class PersonCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var personImage: UIImageView!
    @IBOutlet weak var personName: UILabel!
    @IBOutlet weak var personAddress: UILabel!
    @IBOutlet weak var personPhoneNumber: UILabel!
    
    var imageCache = ImageCache()
    
    func configure(object: PersonInfo) {
        guard let name = object.name else {return}
        let fullName = "\(name.first ?? "")" + " " + "\(name.last ?? "")"
        personName.text = fullName
        
        guard let adress = object.location else {return}
        guard let country = adress.country else {return}
        personAddress.text = country
        
        guard let phoneNumber = object.phone else {return}
        personPhoneNumber.text = phoneNumber
        
        guard let image = object.picture else {return}
        guard let mediumImage = image.medium else {return}
        if let imageUrl = URL(string: mediumImage) {
            imageCache.downloadImage(url: imageUrl) { image in
                DispatchQueue.main.async {
                    self.personImage.image = image
                    self.personImage.cornerRadius(radius: Int(self.personImage.frame.width) / 2)
                }
            }
        }
    }
}
